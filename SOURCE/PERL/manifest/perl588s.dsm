#
# DSM for Perl 5.8.8 sources
# Written by Juan Manuel Guerrero <juan.guerrero@gmx.de> 2007-05-11
#
# This port is partially based on the previuos ports of Laszlo Molnar,
# Richard Dawe and Tim Van Holder.
#

dsm-file-version: 1.0
dsm-name: perl588s
dsm-version: 0.6.1

type: sources

dsm-author: Juan Manuel Guerrero
dsm-author-email: juan.guerrero@gmx.de

name: perl
version: 5.8.8 release 1
manifest: perl588s
binaries-dsm: perl588b
documentation-dsm: 
short-description: Sources for Perl version 5.8.8
long-description: Perl, the Practical Extraction and Report Language.\n\
Perl is a language that combines some of the features of C, sed, awk\n\
and shell.

license: Artistic License or GNU General Public License (version 1 or later)
author: Larry Wall
author-email: larry@wall.org
maintainer: Larry Wall
maintainer-email: larry@wall.org
# For reporting bugs
mailing-list: jseward@bzip.org
# The home page of Perl.
web-site: http://www.perl.org
# The ftp site of Perl.
ftp-site: ftp://ftp.perl.org
# For discussing DJGPP- and DOS/Windows-specific aspects
mailing-list: djgpp@delorie.com
newsgroup: comp.os.msdos.djgpp

porter: Juan Manuel Guerrero
porter-email: juan.guerrero@gmx.de

simtelnet-path: v2apps/
zip: perl588b.zip
changelog: gnu/perl-5.8-8/Changes

post-install-readme: gnu/perl-5.8-8/Readme
post-install-readme: gnu/perl-5.8-8/README.dos
post-install-readme: gnu/perl-5.8-8/djgpp/readme

# These are required to build Bzip2:
requires: DPMI 0.9
requires: gcc
requires: binutils
requires: djdev
requires: sed
requires: diffs
requires: make >= 3.75
requires: bash
requires: fileutils
requires: textutils
requires: sh-utils
requires: gawk

replaces: perl < 5.8.8 release 1

# DJGPP v2.02 or later is need, because in older versions
# `pathconf' didn't pay attention to LFN support.
depends-on: djdev >= 2.03
# For GNU install, to run "make install".  Also, for "make clean".
depends-on: fileutils
# For GNU Bash and GNU `echo', to run "make install".
depends-on: bash
depends-on: sh-utils
# For GNU diff and GNU cmp, to run the checks:
depends-on: diffs
